# Survey Project Server 
This is demo for survey project which is built based on java8/spring boot/maven/docker

- - -
## Tech Stack
* [Java8](www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html)
* [Spring Boot](https://projects.spring.io/spring-boot)
* [Maven](https://maven.apache.org)
* [Postgres](https://www.postgresql.org/)
* [JUnit5](https://junit.org/junit5)
* [Mybatis](http://www.mybatis.org/mybatis-3)
* [Swagger](http://springfox.github.io/springfox/docs/current)
* [JSON API](http://jsonapi.org/recommendations)
* [Docker](https://www.docker.com)

## Installation
#### Build jar package
Please install java8/maven/git on your laptop before start
```sh
git clone https://gitlab.com/my-survey-project/survey-api.git
cd survey-api
mvn clean package -U -B
```
#### Run application
```sh
java -jar build/target/survey-api-1.0.0.jar
```
#### Build docker image and run by using docker-compose
```sh
docker-compose up -d
```
#### Access API Document
[http://localhost:8080/swagger-ui.html](http://localhost:8000/swagger-ui.html)
- - -
#### Unit Test
Execute `mvn clean test` to run unit test
- - -

