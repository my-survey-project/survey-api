package com.survey.api;

import com.survey.model.Survey;
import com.survey.model.SurveyAnswer;
import com.survey.repository.SurveyRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SurveyResultApiControllerTest {

    @InjectMocks
    private SurveyResultApiController surveyResultApiController;

    @Mock
    private SurveyRepository surveyRepository;

    @Test
    void testAddModelApi() {
        when(surveyRepository.findSurveyAnswersBySurveyId(Mockito.anyInt())).thenReturn(
                Collections.singletonList(new SurveyAnswer()));
        when(surveyRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(new Survey()));
        assertNotNull(surveyResultApiController.getSurveyResult(Mockito.anyInt()));
    }
}