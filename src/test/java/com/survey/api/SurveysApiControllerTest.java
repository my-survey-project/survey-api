package com.survey.api;

import com.survey.config.SimpleLoginUser;
import com.survey.model.Survey;
import com.survey.model.User;
import com.survey.repository.SurveyRepository;
import com.survey.services.SurveyService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import sun.jvm.hotspot.debugger.Page;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SurveysApiControllerTest {

    @Mock
    private SurveyRepository surveyRepository;

    @Mock
    private SurveyService surveyService;

    @InjectMocks
    private SurveysApiController surveysApiController;

    private final User user = new User();

    @BeforeEach
    void init() {
        user.setRole(User.RoleEnum.USER);
        user.setEmail("test@mail.com");
        user.setId(0);
        user.setPassword("123");
        user.setUsername("test user");
    }

    @Test
    void answerSurvey() {
        assertNotNull(surveysApiController.answerSurvey(Mockito.anyInt(), Mockito.any()));
    }

    @Test
    void createSurvey() {
        assertNotNull(surveysApiController.createSurvey(new SimpleLoginUser(user), new Survey()));
    }

    @Test
    void deleteSurvey() {
        assertNotNull(surveysApiController.deleteSurvey(new SimpleLoginUser(user), 0));
    }

    @Test
    void getSurvey() {
        when(surveyRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(new Survey()));
        assertNotNull(surveysApiController.getSurvey(0));
    }

    @Test
    void getSurveys() {
        when(surveyService.findSurveys(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(
                new PageImpl<Survey>(Collections.singletonList(new Survey()), PageRequest.of(1, 1), 1));
        assertNotNull(surveysApiController.getSurveys(new SimpleLoginUser(user), 20, 0));
    }

    @Test
    void updateSurvey() {
        assertNotNull(surveysApiController.updateSurvey(new SimpleLoginUser(user), 0, new Survey()));
    }
}