package com.survey.config;

import com.survey.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

public class SimpleLoginUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public User getUser() {
        return user;
    }

    public SimpleLoginUser(User user) {
        super(user.getUsername(), user.getPassword(), determineRoles(user.getRole().toString()));
        this.user = user;
    }

    private static final List<GrantedAuthority> USER_ROLES =
            AuthorityUtils.createAuthorityList("ROLE_USER");
    private static final List<GrantedAuthority> ADMIN_ROLES =
            AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER");
    private static final List<GrantedAuthority> SUPER_ADMIN_ROLES =
            AuthorityUtils.createAuthorityList("ROLE_SUPER_ADMIN", "ROLE_ADMIN", "ROLE_USER");

    private static List<GrantedAuthority> determineRoles(String role) {
        switch (role) {
            case "admin":
                return ADMIN_ROLES;
            case "super":
                return SUPER_ADMIN_ROLES;
            default:
                return USER_ROLES;
        }
    }
}
