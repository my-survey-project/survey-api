package com.survey.services;

import com.survey.model.Survey;
import com.survey.repository.SurveyRepository;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class SurveyService {
    @Autowired
    private SurveyRepository surveyRepository;

    public Page<Survey> findSurveys(Integer userId, int offset, int limit) {
        long count = surveyRepository.selectSurveysCount(userId);

        List<Survey> surveyList = Collections.emptyList();
        if (count > 0) {
            surveyList = surveyRepository.findAllWithRowBounds(userId, offset, limit);
        }

        Page<Survey> page = new PageImpl<Survey>(surveyList,  PageRequest.of(offset / limit, limit), count);
        return page;
    }
}
