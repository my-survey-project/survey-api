package com.survey.repository;

import com.survey.model.Survey;
import com.survey.model.SurveyAnswer;
import com.survey.model.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Mapper
public interface SurveyRepository {
    long selectSurveysCount(Integer userId);

    Optional<Survey> findById(Integer surveyId);
    Optional<Survey> findByUserIdAndSurveyId(Integer userId, Integer surveyId);
    List<Survey> findAllWithRowBounds(Integer userId, int offset, int limit);

    void insertSurvey(Integer userId, Survey survey);
    void updateSurvey(Integer userId, Integer surveyId, Survey survey);
    void deleteSurvey(Integer userId, Integer surveyId);

    Collection<SurveyAnswer> findSurveyAnswersBySurveyId(Integer surveyId);
    void insertSurveyAnswer(Integer surveyId, SurveyAnswer surveyAnswer);
}
