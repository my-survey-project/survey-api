package com.survey.repository;

import com.survey.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Optional;

@Mapper
public interface UserRepository {
    // Search operations
    Optional<User> findByEmail(String email);
    Optional<User> findByUserIdWithoutPassword(Integer userId);
    Optional<User> findByEmailAndPassword(String email, String password);

    // Insert operation
    void insertUser(User user);

    // Update operation
    void updateUserByUserId(Integer userId, User user);
}
