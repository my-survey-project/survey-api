package com.survey.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Answer
 */

public class Answer   {
  @JsonProperty("questionIdx")
  private Integer questionIdx;

  @JsonProperty("optionIdx")
  private Integer optionIdx;

  public Answer questionIdx(Integer questionIdx) {
    this.questionIdx = questionIdx;
    return this;
  }

  /**
   * Get questionIdx
   * @return questionIdx
  */
  @ApiModelProperty(value = "")


  public Integer getQuestionIdx() {
    return questionIdx;
  }

  public void setQuestionIdx(Integer questionIdx) {
    this.questionIdx = questionIdx;
  }

  public Answer optionIdx(Integer optionIdx) {
    this.optionIdx = optionIdx;
    return this;
  }

  /**
   * Get optionIdx
   * @return optionIdx
  */
  @ApiModelProperty(value = "")


  public Integer getOptionIdx() {
    return optionIdx;
  }

  public void setOptionIdx(Integer optionIdx) {
    this.optionIdx = optionIdx;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Answer answer = (Answer) o;
    return Objects.equals(this.questionIdx, answer.questionIdx) &&
        Objects.equals(this.optionIdx, answer.optionIdx);
  }

  @Override
  public int hashCode() {
    return Objects.hash(questionIdx, optionIdx);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Answer {\n");
    
    sb.append("    questionIdx: ").append(toIndentedString(questionIdx)).append("\n");
    sb.append("    optionIdx: ").append(toIndentedString(optionIdx)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

