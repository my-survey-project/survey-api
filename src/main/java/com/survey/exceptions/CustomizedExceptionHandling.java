package com.survey.exceptions;

import com.survey.model.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomizedExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserAlreadyExistAuthenticationException.class)
    public ResponseEntity<?> handleExceptions(UserAlreadyExistAuthenticationException exception, WebRequest webRequest) {
        ExceptionResponse response = new ExceptionResponse();
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Username already exists");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameOrPasswordNotCorrectException.class)
    public ResponseEntity<?> handleExceptions(UsernameOrPasswordNotCorrectException exception, WebRequest webRequest) {
        ExceptionResponse response = new ExceptionResponse();
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Username or password not correct");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TryingToGetOtherUserInfoException.class)
    public ResponseEntity<?> handleExceptions(TryingToGetOtherUserInfoException exception, WebRequest webRequest) {
        ExceptionResponse response = new ExceptionResponse();
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Trying to get other user's information");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TryingToUpdateOtherUserInfoException.class)
    public ResponseEntity<?> handleExceptions(TryingToUpdateOtherUserInfoException exception, WebRequest webRequest) {
        ExceptionResponse response = new ExceptionResponse();
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Trying to update other user's information");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SurveyNotExistException.class)
    public ResponseEntity<?> handleExceptions(SurveyNotExistException exception, WebRequest webRequest) {
        ExceptionResponse response = new ExceptionResponse();
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Survey not exists");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
