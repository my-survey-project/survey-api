package com.survey.api;

import com.survey.config.SimpleLoginUser;
import com.survey.exceptions.TryingToGetOtherUserInfoException;
import com.survey.exceptions.TryingToUpdateOtherUserInfoException;
import com.survey.exceptions.UserAlreadyExistAuthenticationException;
import com.survey.exceptions.UsernameOrPasswordNotCorrectException;
import com.survey.model.GenericErrorModel;
import com.survey.model.User;
import com.survey.repository.UserRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@Controller
@RequestMapping("${openapi.survey.base-path:}")
public class UsersApiController implements UsersApi {

    private final NativeWebRequest request;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(NativeWebRequest request, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.request = request;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    /**
     * POST /users/register : Register a new user
     * Register a new user
     *
     * @param user Details of the new user to register (required)
     * @return OK (status code 201)
     * or Unexpected error (status code 400)
     */
    @ApiOperation(value = "Register a new user", nickname = "createUser", notes = "Register a new user", tags = {"Users and Authentication",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class)})
    @RequestMapping(value = "/users/register",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@ApiParam(value = "Details of the new user to register", required = true) @Valid @RequestBody User user) {
        Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
        if (optionalUser.isPresent()) {
            throw new UserAlreadyExistAuthenticationException();
        }

        // Initialize the user data
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(User.RoleEnum.USER);

        userRepository.insertUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * GET /users/{userId} : Get user information by id
     * Gets other user information
     *
     * @param userId id of the user to get (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Get user information by id", nickname = "getUserInfo", notes = "Gets user information", response = User.class, tags = {"Users and Authentication",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = User.class),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/users/{userId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    public ResponseEntity<?> getUserInfo(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "id of the user to get", required = true) @PathVariable("userId") String userId) {
        return ResponseEntity.ok().body(user);
    }


    /**
     * POST /users/login : Existing user login
     * Login for existing user
     *
     * @param email    (required)
     * @param password (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Existing user login", nickname = "login", notes = "Login for existing user", tags = {"Users and Authentication",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/users/login",
            produces = {"application/json"},
            consumes = {"application/x-www-form-urlencoded"},
            method = RequestMethod.POST)
    public ResponseEntity<?> login(@ApiParam(value = "", required = true) @RequestPart(value = "email", required = true) String email, @ApiParam(value = "", required = true) @RequestPart(value = "password", required = true) String password) {
        return Optional
                .ofNullable(userRepository.findByEmailAndPassword(email, password))
                .map(user -> ResponseEntity.ok().body(user))
                .orElseThrow(UsernameOrPasswordNotCorrectException::new);
    }


    /**
     * POST /users/logout : Existing user logout
     * logout for existing user
     *
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Existing user logout", nickname = "logout", notes = "logout for existing user", tags = {"Users and Authentication",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/users/logout",
            produces = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<Void> logout() {
        return ResponseEntity.ok().build();
    }


    /**
     * PUT /users/{userId} : Update user
     * Updated user information
     *
     * @param userId id of the user to get (required)
     * @param user   User details to update. At least **one** field is required. (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Update user", nickname = "updateUserInfo", notes = "Updated user information", tags = {"Users and Authentication",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/users/{userId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    public ResponseEntity<Void> updateUserInfo(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "id of the user to get", required = true) @PathVariable("userId") String userId, @ApiParam(value = "User details to update. At least **one** field is required.", required = true) @Valid @RequestBody User newUser) {
        userRepository.updateUserByUserId(user.getUser().getId(), newUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
