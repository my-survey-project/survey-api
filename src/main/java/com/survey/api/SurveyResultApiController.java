package com.survey.api;

import com.survey.config.SimpleLoginUser;
import com.survey.exceptions.SurveyNotExistException;
import com.survey.model.GenericErrorModel;
import com.survey.model.Survey;
import com.survey.model.SurveyAnswer;
import com.survey.model.User;
import com.survey.repository.SurveyRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("${openapi.survey.base-path:}")
public class SurveyResultApiController implements SurveyResultApi {

    private final NativeWebRequest request;

    private final SurveyRepository surveyRepository;

    @org.springframework.beans.factory.annotation.Autowired
    public SurveyResultApiController(NativeWebRequest request, SurveyRepository surveyRepository) {
        this.request = request;
        this.surveyRepository = surveyRepository;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    /**
     * GET /surveyResult/{surveyId} : Get result of survey
     * Get result of survey. Auth is required
     *
     * @param surveyId ID of the survey that you want to get (required)
     * @return OK (status code 200)
     *         or Unexpected error (status code 400)
     *         or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Get result of survey", nickname = "getSurveyResult", notes = "Get result of survey. Auth is required", response = SurveyAnswer.class, responseContainer = "List", tags={ "Survey", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = SurveyAnswer.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized") })
    @RequestMapping(value = "/surveyResult/{surveyId}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyResult(@ApiParam(value = "ID of the survey that you want to get",required=true) @PathVariable("surveyId") Integer surveyId) {
        Optional<Survey> optionalSurvey = surveyRepository.findById(surveyId);
        if (!optionalSurvey.isPresent()) {
            throw new SurveyNotExistException();
        }

        return Optional
                .ofNullable(surveyRepository.findSurveyAnswersBySurveyId(surveyId))
                .map(surveyAnswers -> ResponseEntity.ok().body(surveyAnswers))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
