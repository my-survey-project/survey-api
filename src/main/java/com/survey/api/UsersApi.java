/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (4.3.1).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package com.survey.api;

import com.survey.config.SimpleLoginUser;
import com.survey.model.GenericErrorModel;
import com.survey.model.User;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Validated
@Api(value = "users", description = "the users API")
public interface UsersApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /users/register : Register a new user
     * Register a new user
     *
     * @param user Details of the new user to register (required)
     * @return OK (status code 201)
     *         or Unexpected error (status code 400)
     */
    @ApiOperation(value = "Register a new user", nickname = "createUser", notes = "Register a new user", tags={ "Users and Authentication", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "OK"),
        @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class) })
    @RequestMapping(value = "/users/register",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<Void> createUser(@ApiParam(value = "Details of the new user to register" ,required=true )  @Valid @RequestBody User user) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /users/{userId} : Get user information by id
     * Gets other user information
     *
     * @param userId id of the user to get (required)
     * @return OK (status code 200)
     *         or Unexpected error (status code 400)
     *         or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Get user information by id", nickname = "getUserInfo", notes = "Gets other user information", response = User.class, tags={ "Users and Authentication", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = User.class),
        @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
        @ApiResponse(code = 401, message = "Unauthorized") })
    @RequestMapping(value = "/users/{userId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<?> getUserInfo(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "id of the user to get",required=true) @PathVariable("userId") String userId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"password\" : \"hard%to@guess\", \"role\" : \"user\", \"createTime\" : \"2018-11-23T04:56:07Z\", \"updateTime\" : \"2018-11-23T04:56:07Z\", \"id\" : 123, \"email\" : \"test.taro@gmail.com\", \"username\" : \"TestUser\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * POST /users/login : Existing user login
     * Login for existing user
     *
     * @param email  (required)
     * @param password  (required)
     * @return OK (status code 200)
     *         or Unexpected error (status code 400)
     *         or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Existing user login", nickname = "login", notes = "Login for existing user", tags={ "Users and Authentication", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
        @ApiResponse(code = 401, message = "Unauthorized") })
    @RequestMapping(value = "/users/login",
        produces = { "application/json" }, 
        consumes = { "application/x-www-form-urlencoded" },
        method = RequestMethod.POST)
    default ResponseEntity<?> login(@ApiParam(value = "", required=true) @RequestPart(value="email", required=true)  String email,@ApiParam(value = "", required=true) @RequestPart(value="password", required=true)  String password) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * POST /users/logout : Existing user logout
     * logout for existing user
     *
     * @return OK (status code 200)
     *         or Unexpected error (status code 400)
     *         or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Existing user logout", nickname = "logout", notes = "logout for existing user", tags={ "Users and Authentication", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
        @ApiResponse(code = 401, message = "Unauthorized") })
    @RequestMapping(value = "/users/logout",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    default ResponseEntity<Void> logout() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * PUT /users/{userId} : Update user
     * Updated user information
     *
     * @param userId id of the user to get (required)
     * @param user User details to update. At least **one** field is required. (required)
     * @return OK (status code 200)
     *         or Unexpected error (status code 400)
     *         or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Update user", nickname = "updateUserInfo", notes = "Updated user information", tags={ "Users and Authentication", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
        @ApiResponse(code = 401, message = "Unauthorized") })
    @RequestMapping(value = "/users/{userId}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    default ResponseEntity<Void> updateUserInfo(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "id of the user to get", required = true) @PathVariable("userId") String userId, @ApiParam(value = "User details to update. At least **one** field is required.", required = true) @Valid @RequestBody User newUser) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
