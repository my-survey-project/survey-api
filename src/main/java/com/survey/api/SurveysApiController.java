package com.survey.api;

import com.survey.config.SimpleLoginUser;
import com.survey.model.*;
import com.survey.repository.SurveyRepository;
import com.survey.services.SurveyService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.ibatis.session.RowBounds;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("${openapi.survey.base-path:}")
public class SurveysApiController implements SurveysApi {

    private final NativeWebRequest request;

    private final SurveyRepository surveyRepository;

    private final SurveyService surveyService;

    @org.springframework.beans.factory.annotation.Autowired
    public SurveysApiController(NativeWebRequest request, SurveyRepository surveyRepository, SurveyService surveyService) {
        this.request = request;
        this.surveyRepository = surveyRepository;
        this.surveyService = surveyService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    /**
     * POST /surveys/{surveyId} : Answer a survey
     * Answer a survey
     *
     * @param surveyId     id of the survey to answer (required)
     * @param surveyAnswer Survey details. (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Answer a survey", nickname = "answerSurvey", notes = "Answer a survey", tags = {"Survey",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/surveys/{surveyId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<Void> answerSurvey(@ApiParam(value = "id of the survey to answer", required = true) @PathVariable("surveyId") Integer surveyId, @ApiParam(value = "Survey details.", required = true) @Valid @RequestBody SurveyAnswer surveyAnswer) {
        this.surveyRepository.insertSurveyAnswer(surveyId, surveyAnswer);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * POST /surveys : Create a survey
     * Create a survey
     *
     * @param survey Survey details. (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Create a survey", nickname = "createSurvey", notes = "Create a survey", tags = {"Survey",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/surveys",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<Void> createSurvey(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "Survey details.", required = true) @Valid @RequestBody Survey survey) {
        this.surveyRepository.insertSurvey(user.getUser().getId(), survey);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * DELETE /surveys/{surveyId} : Delete a survey
     * Delete a survey. Auth is required
     *
     * @param surveyId ID of the survey that you want to delete (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Delete a survey", nickname = "deleteSurvey", notes = "Unfavorite a survey. Auth is required", tags = {"Survey",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/surveys/{surveyId}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteSurvey(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "ID of the survey that you want to delete", required = true) @PathVariable("surveyId") Integer surveyId) {
        this.surveyRepository.deleteSurvey(user.getUser().getId(), surveyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * GET /surveys/{surveyId} : Get survey detail
     * Get survey detail data. Auth not required
     *
     * @param surveyId id of the survey to get (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     */
    @ApiOperation(value = "Get survey detail", nickname = "getSurvey", notes = "Get survey detail data. Auth not required", response = Survey.class, tags = {"Survey",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Survey.class),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class)})
    @RequestMapping(value = "/surveys/{surveyId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    public ResponseEntity<?> getSurvey(@ApiParam(value = "id of the survey to get", required = true) @PathVariable("surveyId") Integer surveyId) {
        return Optional
                .ofNullable(surveyRepository.findById(surveyId))
                .map(survey -> ResponseEntity.ok().body(survey))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    /**
     * GET /surveys : Get all surveys
     * Get surveys. Auth is required
     *
     * @param limit  Limit number of comments returned (default is 20) (optional, default to 20)
     * @param offset Offset/skip number of comments (default is 0) (optional, default to 0)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     */
    @ApiOperation(value = "Get all surveys", nickname = "getSurveys", notes = "Get surveys. Auth is required", response = PageSurvey.class, tags = {"Survey",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PageSurvey.class),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class)})
    @RequestMapping(value = "/surveys",
            produces = {"application/json"},
            method = RequestMethod.GET)
    public ResponseEntity<?> getSurveys(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "Limit number of comments returned (default is 20)", defaultValue = "20") @Valid @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit, @ApiParam(value = "Offset/skip number of comments (default is 0)", defaultValue = "0") @Valid @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset) {
        Page<Survey> surveys = surveyService.findSurveys(user.getUser().getId(), offset, limit);
        return ResponseEntity.ok().body(surveys);
    }


    /**
     * PUT /surveys/{surveyId} : Update a survey
     * Update a survey. Auth is required
     *
     * @param surveyId ID of the survey that you want to update (required)
     * @return OK (status code 200)
     * or Unexpected error (status code 400)
     * or Unauthorized (status code 401)
     */
    @ApiOperation(value = "Update a survey", nickname = "updateSurvey", notes = "Update a survey. Auth is required", tags = {"Survey",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Unexpected error", response = GenericErrorModel.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/surveys/{surveyId}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    public ResponseEntity<Void> updateSurvey(@AuthenticationPrincipal final SimpleLoginUser user, @ApiParam(value = "ID of the survey that you want to update", required = true) @PathVariable("surveyId") Integer surveyId, @ApiParam(value = "Survey details.", required = true) @Valid @RequestBody Survey survey) {
        surveyRepository.updateSurvey(user.getUser().getId(), surveyId, survey);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
