create table if not exists users (
    id SERIAL PRIMARY KEY,
    username varchar(50) NOT NULL,
    email varchar(50) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(50) NOT NULL,
    created_time timestamp default current_timestamp
);

create table if not exists surveys (
    id SERIAL PRIMARY KEY,
    user_id int NOT NULL,
    title varchar(255) NOT NULL,
    description varchar(1000) NOT NULL,
    questions text,
    created_time timestamp default current_timestamp,
    foreign key (user_id) references public.users(id)
);

create table if not exists survey_answers (
    id SERIAL PRIMARY KEY,
    survey_id int NOT NULL,
    username varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    answers text,
    created_time timestamp default current_timestamp,
    foreign key (survey_id) references public.surveys(id) ON UPDATE NO ACTION ON DELETE CASCADE
);